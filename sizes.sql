UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 3 AND Name = 'Cargo Hold - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5kt)' WHERE SDComponentID = 3 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(5kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 3 AND instr(Name, '(') AND instr(Name, '(5kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 8 AND Name = 'Crew Quarters';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 8 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 8 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 479 AND Name = 'Cryogenic Transport';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (2.5kt)' WHERE SDComponentID = 479 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(2.5kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 479 AND instr(Name, '(') AND instr(Name, '(2.5kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 600 AND Name = 'Fuel Storage';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 600 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 600 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 728 AND Name = 'Troop Transport Bay - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5kt)' WHERE SDComponentID = 728 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(5kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 728 AND instr(Name, '(') AND instr(Name, '(5kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-1' WHERE SDComponentID = 12017 AND Name = 'ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12017 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12017 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-2' WHERE SDComponentID = 12018 AND Name = 'ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12018 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12018 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-3' WHERE SDComponentID = 12019 AND Name = 'ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12019 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12019 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-4' WHERE SDComponentID = 12020 AND Name = 'ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12020 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12020 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-5' WHERE SDComponentID = 12021 AND Name = 'ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12021 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12021 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-6' WHERE SDComponentID = 12022 AND Name = 'ECM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12022 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12022 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-7' WHERE SDComponentID = 12023 AND Name = 'ECM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12023 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12023 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-8' WHERE SDComponentID = 12024 AND Name = 'ECM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12024 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12024 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-9' WHERE SDComponentID = 12025 AND Name = 'ECM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12025 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12025 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-10' WHERE SDComponentID = 12026 AND Name = 'ECM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12026 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12026 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-1' WHERE SDComponentID = 12027 AND Name = 'ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12027 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12027 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-2' WHERE SDComponentID = 12028 AND Name = 'ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12028 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12028 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-3' WHERE SDComponentID = 12029 AND Name = 'ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12029 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12029 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-4' WHERE SDComponentID = 12030 AND Name = 'ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12030 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12030 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-5' WHERE SDComponentID = 12031 AND Name = 'ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12031 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12031 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-6' WHERE SDComponentID = 12032 AND Name = 'ECCM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12032 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12032 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-7' WHERE SDComponentID = 12033 AND Name = 'ECCM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12033 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12033 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-8' WHERE SDComponentID = 12034 AND Name = 'ECCM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12034 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12034 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-9' WHERE SDComponentID = 12035 AND Name = 'ECCM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12035 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12035 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-10' WHERE SDComponentID = 12036 AND Name = 'ECCM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 12036 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12036 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-1' WHERE SDComponentID = 12037 AND Name = 'Compact ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12037 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12037 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-2' WHERE SDComponentID = 12038 AND Name = 'Compact ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12038 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12038 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-3' WHERE SDComponentID = 12039 AND Name = 'Compact ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12039 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12039 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-4' WHERE SDComponentID = 12040 AND Name = 'Compact ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12040 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12040 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-5' WHERE SDComponentID = 12041 AND Name = 'Compact ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12041 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12041 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-6' WHERE SDComponentID = 12042 AND Name = 'Compact ECM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12042 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12042 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-7' WHERE SDComponentID = 12043 AND Name = 'Compact ECM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12043 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12043 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-8' WHERE SDComponentID = 12044 AND Name = 'Compact ECM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12044 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12044 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-9' WHERE SDComponentID = 12045 AND Name = 'Compact ECM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12045 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12045 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-10' WHERE SDComponentID = 12046 AND Name = 'Compact ECM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12046 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12046 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-1' WHERE SDComponentID = 12047 AND Name = 'Compact ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12047 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12047 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-2' WHERE SDComponentID = 12048 AND Name = 'Compact ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12048 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12048 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-3' WHERE SDComponentID = 12049 AND Name = 'Compact ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12049 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12049 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-4' WHERE SDComponentID = 12050 AND Name = 'Compact ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12050 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12050 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-5' WHERE SDComponentID = 12051 AND Name = 'Compact ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12051 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12051 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-6' WHERE SDComponentID = 12052 AND Name = 'Compact ECCM-6';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12052 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12052 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-7' WHERE SDComponentID = 12053 AND Name = 'Compact ECCM-7';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12053 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12053 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-8' WHERE SDComponentID = 12054 AND Name = 'Compact ECCM-8';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12054 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12054 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-9' WHERE SDComponentID = 12055 AND Name = 'Compact ECCM-9';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12055 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12055 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-10' WHERE SDComponentID = 12056 AND Name = 'Compact ECCM-10';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 12056 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 12056 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 180days' WHERE SDComponentID = 24990 AND Name = 'Jump Point Stabilisation Module - 180';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50kt)' WHERE SDComponentID = 24990 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24990 AND instr(Name, '(') AND instr(Name, '(50kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 150days' WHERE SDComponentID = 24993 AND Name = 'Jump Point Stabilisation Module - 150';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50kt)' WHERE SDComponentID = 24993 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24993 AND instr(Name, '(') AND instr(Name, '(50kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 120days' WHERE SDComponentID = 24994 AND Name = 'Jump Point Stabilisation Module - 120';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (55kt)' WHERE SDComponentID = 24994 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(55kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24994 AND instr(Name, '(') AND instr(Name, '(55kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 90days' WHERE SDComponentID = 24995 AND Name = 'Jump Point Stabilisation Module - 90';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (60kt)' WHERE SDComponentID = 24995 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(60kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24995 AND instr(Name, '(') AND instr(Name, '(60kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 60days' WHERE SDComponentID = 24996 AND Name = 'Jump Point Stabilisation Module - 60';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (65kt)' WHERE SDComponentID = 24996 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(65kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24996 AND instr(Name, '(') AND instr(Name, '(65kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 45days' WHERE SDComponentID = 24997 AND Name = 'Jump Point Stabilisation Module - 45';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (70kt)' WHERE SDComponentID = 24997 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(70kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24997 AND instr(Name, '(') AND instr(Name, '(70kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 30days' WHERE SDComponentID = 24998 AND Name = 'Jump Point Stabilisation Module - 30';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (80kt)' WHERE SDComponentID = 24998 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(80kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24998 AND instr(Name, '(') AND instr(Name, '(80kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 20days' WHERE SDComponentID = 24999 AND Name = 'Jump Point Stabilisation Module - 20';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (90kt)' WHERE SDComponentID = 24999 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(90kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 24999 AND instr(Name, '(') AND instr(Name, '(90kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 25147 AND Name = 'Engineering Spaces';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 25147 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 25147 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 26265 AND Name = 'Crew Quarters - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (10t)' WHERE SDComponentID = 26265 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(10t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26265 AND instr(Name, '(') AND instr(Name, '(10t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 26266 AND Name = 'Fuel Storage - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (10t)' WHERE SDComponentID = 26266 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(10t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26266 AND instr(Name, '(') AND instr(Name, '(10t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 26267 AND Name = 'Engineering Spaces - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26267 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26267 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Hangar Deck' WHERE SDComponentID = 26276 AND Name = 'Hangar Deck';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1.05kt)' WHERE SDComponentID = 26276 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1.05kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26276 AND instr(Name, '(') AND instr(Name, '(1.05kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-5' WHERE SDComponentID = 26354 AND Name = 'Small Craft ECM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26354 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26354 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-1' WHERE SDComponentID = 26355 AND Name = 'Small Craft ECM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26355 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26355 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-2' WHERE SDComponentID = 26357 AND Name = 'Small Craft ECM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26357 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26357 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-3' WHERE SDComponentID = 26359 AND Name = 'Small Craft ECM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26359 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26359 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECM-4' WHERE SDComponentID = 26361 AND Name = 'Small Craft ECM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26361 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26361 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-5' WHERE SDComponentID = 26363 AND Name = 'Small Craft ECCM-5';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26363 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26363 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-1' WHERE SDComponentID = 26364 AND Name = 'Small Craft ECCM-1';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26364 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26364 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-2' WHERE SDComponentID = 26365 AND Name = 'Small Craft ECCM-2';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26365 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26365 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-3' WHERE SDComponentID = 26366 AND Name = 'Small Craft ECCM-3';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26366 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26366 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'ECCM-4' WHERE SDComponentID = 26367 AND Name = 'Small Craft ECCM-4';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25t)' WHERE SDComponentID = 26367 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26367 AND instr(Name, '(') AND instr(Name, '(25t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 26420 AND Name = 'Compressed Fuel Storage System';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 26420 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 26420 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 27132 AND Name = 'Large Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 27132 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(250t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 27132 AND instr(Name, '(') AND instr(Name, '(250t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 27133 AND Name = 'Engineering Spaces - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (12.5t)' WHERE SDComponentID = 27133 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(12.5t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 27133 AND instr(Name, '(') AND instr(Name, '(12.5t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Engineering Spaces' WHERE SDComponentID = 27134 AND Name = 'Engineering Spaces - Fighter';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5t)' WHERE SDComponentID = 27134 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(5t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 27134 AND instr(Name, '(') AND instr(Name, '(5t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Jump Point Stabilisation Module 360days' WHERE SDComponentID = 33215 AND Name = 'Small Jump Point Stabilisation Module';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25kt)' WHERE SDComponentID = 33215 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 33215 AND instr(Name, '(') AND instr(Name, '(25kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 33426 AND Name = 'Troop Transport Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1kt)' WHERE SDComponentID = 33426 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 33426 AND instr(Name, '(') AND instr(Name, '(1kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 33433 AND Name = 'Boat Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (300t)' WHERE SDComponentID = 33433 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(300t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 33433 AND instr(Name, '(') AND instr(Name, '(300t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 38117 AND Name = 'Fuel Storage - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5t)' WHERE SDComponentID = 38117 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(5t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 38117 AND instr(Name, '(') AND instr(Name, '(5t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 43528 AND Name = 'Cargo Hold - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (25kt)' WHERE SDComponentID = 43528 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(25kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 43528 AND instr(Name, '(') AND instr(Name, '(25kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 43529 AND Name = 'Fuel Storage - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 43529 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(250t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 43529 AND instr(Name, '(') AND instr(Name, '(250t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 43530 AND Name = 'Fuel Storage - Ultra Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5kt)' WHERE SDComponentID = 43530 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(5kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 43530 AND instr(Name, '(') AND instr(Name, '(5kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 43531 AND Name = 'Fuel Storage - Very Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1kt)' WHERE SDComponentID = 43531 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 43531 AND instr(Name, '(') AND instr(Name, '(1kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 43532 AND Name = 'Cryogenic Transport - Emergency';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 43532 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 43532 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Cryogenic Transport' WHERE SDComponentID = 43533 AND Name = 'Cryogenic Transport - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 43533 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(250t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 43533 AND instr(Name, '(') AND instr(Name, '(250t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 47485 AND Name = 'Crew Quarters - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5t)' WHERE SDComponentID = 47485 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(5t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 47485 AND instr(Name, '(') AND instr(Name, '(5t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 55437 AND Name = 'Troop Transport Drop Bay - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (6kt)' WHERE SDComponentID = 55437 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(6kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 55437 AND instr(Name, '(') AND instr(Name, '(6kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 55438 AND Name = 'Troop Transport Drop Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1.2kt)' WHERE SDComponentID = 55438 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1.2kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 55438 AND instr(Name, '(') AND instr(Name, '(1.2kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Crew Quarters' WHERE SDComponentID = 62453 AND Name = 'Crew Quarters - Fighter';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (2t)' WHERE SDComponentID = 62453 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(2t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 62453 AND instr(Name, '(') AND instr(Name, '(2t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Boat Bay' WHERE SDComponentID = 62489 AND Name = 'Boat Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (150t)' WHERE SDComponentID = 62489 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(150t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 62489 AND instr(Name, '(') AND instr(Name, '(150t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 64796 AND Name = 'Compressed Fuel Storage System - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 64796 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(250t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 64796 AND instr(Name, '(') AND instr(Name, '(250t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 64797 AND Name = 'Compressed Fuel Storage System - Very Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1kt)' WHERE SDComponentID = 64797 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 64797 AND instr(Name, '(') AND instr(Name, '(1kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Compressed Fuel Storage System' WHERE SDComponentID = 65061 AND Name = 'Compressed Fuel Storage System - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (10t)' WHERE SDComponentID = 65061 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(10t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 65061 AND instr(Name, '(') AND instr(Name, '(10t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Commercial Hangar Deck' WHERE SDComponentID = 65297 AND Name = 'Commercial Hangar Deck';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1.6kt)' WHERE SDComponentID = 65297 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1.6kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 65297 AND instr(Name, '(') AND instr(Name, '(1.6kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 65307 AND Name = 'Cargo Hold - Tiny';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (500t)' WHERE SDComponentID = 65307 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(500t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 65307 AND instr(Name, '(') AND instr(Name, '(500t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 65454 AND Name = 'Troop Transport Boarding Bay - Standard';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1.1kt)' WHERE SDComponentID = 65454 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1.1kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 65454 AND instr(Name, '(') AND instr(Name, '(1.1kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 65848 AND Name = 'Troop Transport Boarding Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (275t)' WHERE SDComponentID = 65848 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(275t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 65848 AND instr(Name, '(') AND instr(Name, '(275t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 65849 AND Name = 'Troop Transport Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (100t)' WHERE SDComponentID = 65849 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(100t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 65849 AND instr(Name, '(') AND instr(Name, '(100t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 65850 AND Name = 'Troop Transport Drop Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (120t)' WHERE SDComponentID = 65850 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(120t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 65850 AND instr(Name, '(') AND instr(Name, '(120t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Fuel Storage' WHERE SDComponentID = 67058 AND Name = 'Fuel Storage - Fighter';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (1t)' WHERE SDComponentID = 67058 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(1t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 67058 AND instr(Name, '(') AND instr(Name, '(1t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Cargo Hold' WHERE SDComponentID = 67059 AND Name = 'Cargo Hold - Large';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (125kt)' WHERE SDComponentID = 67059 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(125kt)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 67059 AND instr(Name, '(') AND instr(Name, '(125kt)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Boarding Bay' WHERE SDComponentID = 67060 AND Name = 'Troop Transport Boarding Bay - Very Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (110t)' WHERE SDComponentID = 67060 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(110t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 67060 AND instr(Name, '(') AND instr(Name, '(110t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76178 AND Name = 'Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (50t)' WHERE SDComponentID = 76178 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(50t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 76178 AND instr(Name, '(') AND instr(Name, '(50t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76179 AND Name = 'Small Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (10t)' WHERE SDComponentID = 76179 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(10t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 76179 AND instr(Name, '(') AND instr(Name, '(10t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76180 AND Name = 'Tiny Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (5t)' WHERE SDComponentID = 76180 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(5t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 76180 AND instr(Name, '(') AND instr(Name, '(5t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Maintenance Storage Bay' WHERE SDComponentID = 76181 AND Name = 'Fighter Maintenance Storage Bay';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (2.5t)' WHERE SDComponentID = 76181 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(2.5t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 76181 AND instr(Name, '(') AND instr(Name, '(2.5t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Bay' WHERE SDComponentID = 78585 AND Name = 'Troop Transport Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (250t)' WHERE SDComponentID = 78585 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(250t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 78585 AND instr(Name, '(') AND instr(Name, '(250t)') = 0;

UPDATE FCT_ShipDesignComponents SET Name = 'Troop Transport Drop Bay' WHERE SDComponentID = 78586 AND Name = 'Troop Transport Drop Bay - Small';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' (300t)' WHERE SDComponentID = 78586 AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '(300t)' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = 78586 AND instr(Name, '(') AND instr(Name, '(300t)') = 0;

