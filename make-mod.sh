#!/bin/sh
dir="${1}"
if [ -z "${dir}" ] || [ ! -d "${dir}" ]; then
    echo must supply valid directory name
    exit 1
fi
cd "${dir}" || exit

function getnames () {
    for prefix in "Fuel Storage" "Compressed Fuel Storage System" \
                  "Engineering Spaces" "Crew Quarters" "Cryogenic Transport" \
                  "Cargo Hold" "Troop Transport Bay" "Boat Bay" "Hangar Deck" \
                  "Commercial Hangar Deck" "Troop Transport Boarding Bay" \
                  "Troop Transport Drop Bay"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${prefix}' AS NewName,
       replace(CASE WHEN Size < 20 THEN (Size * 50) || 't' ELSE (Size / 20) || 'kt' END, '.0', '') As Size
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '${prefix}%';
EOF
    done

    for suffix in "Maintenance Storage Bay"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${suffix}' AS NewName,
       replace(CASE WHEN Size < 20 THEN (Size * 50) || 't' ELSE (Size / 20) || 'kt' END, '.0', '') As Size
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${suffix}';
EOF
    done

    for suffix in $(seq --format=ECM-%.0f 1 10) $(seq --format=ECCM-%.0f 1 10); do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${suffix}' AS NewName,
       replace(CASE WHEN Size < 20 THEN (Size * 50) || 't' ELSE (Size / 20) || 'kt' END, '.0', '') As Size
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${suffix}';
EOF
    done

    for infix in "Jump Point Stabilisation Module"; do
        sqlite3 AuroraDB.db <<EOF
.mode tabs
SELECT SDComponentID,
       Name AS OldName,
       '${infix}' || ' ' || cast(ComponentValue AS INTEGER) || 'days' AS NewName,
       replace(CASE WHEN Size < 20 THEN (Size * 50) || 't' ELSE (Size / 20) || 'kt' END, '.0', '') As Size
FROM   FCT_ShipDesignComponents
WHERE  Name LIKE '%${infix}%';
EOF
    done
}

rm -f -- *.sql.tmp *.sql 2>&-
getnames | sort -t $'\t' -k1n | while IFS=$'\t' read -r id old new size; do
    echo "id='${id}', old='${old}', new=${new}, size=${size}"
    if [ "${old}" = "Hangar Deck" ]; then
        # this is a stupid hack…
        size="(1.05kt)"
    else
        size="(${size})"
    fi

    cat <<EOF >> sizes.sql
UPDATE FCT_ShipDesignComponents SET Name = '${new}' WHERE SDComponentID = ${id} AND Name = '${old}';
UPDATE FCT_ShipDesignComponents SET Name = Name || ' ${size}' WHERE SDComponentID = ${id} AND instr(Name, '(') = 0;
UPDATE FCT_ShipDesignComponents SET Name = substr(Name, 0, instr(Name, '(')) || '${size}' || substr(Name, instr(Name, '(') - 1) WHERE SDComponentID = ${id} AND instr(Name, '(') AND instr(Name, '${size}') = 0;

EOF
    cat <<EOF >> uninstall.sql
UPDATE FCT_ShipDesignComponents SET Name = replace(Name, ' ${size}', '') WHERE SDComponentID = ${id};
UPDATE FCT_ShipDesignComponents SET Name = '${old}' WHERE SDComponentID = ${id} AND Name = '${new}';

EOF
done
